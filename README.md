# Master GEOPRISME - Analyse spatiale
- Séance 2021 : [les données de la science](https://framagit.org/MarionMai/geoprisme/-/tree/Seance-2021)
- Séance 2022 et TP avec comtradr : [les réseaux de lieux](https://framagit.org/MarionMai/geoprisme/-/tree/Seance-2022) 
- Séance 2023 et TP avec tradestatistics : [les réseaux de lieux](https://framagit.org/MarionMai/geoprisme/-/tree/Seance-2023) 
- Séance 2024 et TP avec tradestatistics : [les réseaux de lieux](https://framagit.org/MarionMai/geoprisme/-/tree/Seance-2024) 
